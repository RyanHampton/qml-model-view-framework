import QtQuick 2.12
import QtQuick.Window 2.12

Window {
    visible: true
    width: 350; height: 250; color: "gray"
    title: qsTr("Contact List")

    Rectangle {
        anchors.centerIn: parent
        width: 300; height: 200; color: "white"

        ListModel {
            id: nameModel
            ListElement { name: "Ryan"; file: "images/star.svg" }
            ListElement { name: "Chris"; file: "images/night.svg" }
            ListElement { name: "Harry"; file: "images/star.svg" }
            ListElement { name: "Wendy"; file: "images/night.svg" }
            ListElement { name: "Bob"; file: "images/star.svg" }
            ListElement { name: "Alice"; file: "images/night.svg" }
            ListElement { name: "Jane"; file: "images/star.svg" }
            ListElement { name: "Kate"; file: "images/star.svg" }
            ListElement { name: "Theo"; file: "images/star.svg" }
        }

        Component {
            id: nameDelegate

            Item {
                id: delegateItem
                width: parent.width
                height: 28

                Text {
                    text: name
                    font.pixelSize: parent.height - 4
                    anchors.left: parent.left
                    anchors.verticalCenter: parent.verticalCenter
                }

                Image {
                    source: file
                    fillMode: Image.PreserveAspectFit
                    smooth: true
                    sourceSize: Qt.size(64,64)

                    width: height; height: parent.height - 4
                    anchors.right: parent.right
                    anchors.verticalCenter: parent.verticalCenter
                }
            }
        }

        ListView {
            id: listView
            anchors.fill: parent
            focus: true
            clip: true

            model: nameModel
            delegate: nameDelegate

            header: Rectangle {
                width: parent.width; height: 10;
                color: "#8080ff"
            }
            footer: Rectangle {
                width: parent.width; height: 10;
                color: "#8080ff"
            }
            highlight: Rectangle {
                // no need to specify geometry
                color: "lightgray"
            }

            Component.onCompleted: {
                listView.forceActiveFocus()
            }
        }
    }

}
