import QtQuick 2.12
import QtQuick.Controls 2.5
import QtQuick.Layouts 1.12
import DownloadManager 1.0
import QtQuick.Dialogs 1.2

RowLayout {
    spacing: 10

    CheckBox {
        checked: model.selected
        onClicked: model.selected = checked
    }

    Label {
        Layout.fillWidth: true
        text: model.error ? qsTr("Error on Download") : model.url
        elide: Text.ElideLeft

        MouseArea {
            id: mp3ItemMouseArea
            anchors.fill: parent
            hoverEnabled: true
            cursorShape: Qt.IBeamCursor
            acceptedButtons: Qt.NoButton
        }
    }

    Label {
        text: model.error ? "0%" : (model.progress * 100).toFixed(0) + "%"
        font.pixelSize: 20
        font.bold: true
    }

    ProgressBar {
        Layout.preferredWidth: 200
        value: model.error ? 0.0 : model.progress
    }

}
