#include "mp3listmodel.h"

static bool double_equals(double a, double b, double epsilon = 0.0001)
{
    return std::abs(a - b) < epsilon;
}


Mp3ListModel::Mp3ListModel(QUrl downloadDir, QObject *parent) noexcept
    : QAbstractListModel(parent), m_downloadDir(downloadDir)
{}

QHash<int, QByteArray> Mp3ListModel::roleNames() const noexcept
{
    QHash<int, QByteArray> roles;
    roles[SelectedRole] = "selected";
    roles[DoneRole] = "done";
    roles[ProgressRole] = "progress";
    roles[UrlRole] = "url";
    roles[ErrorRole] = "error";
    return roles;
}

int Mp3ListModel::rowCount(const QModelIndex &parent) const noexcept
{
    if (parent.isValid())
        return 0;

    return m_mp3Items.size();
}

QVariant Mp3ListModel::data(const QModelIndex &index, int role) const noexcept
{
    if (!index.isValid())
        return QVariant();

    Mp3Item *mp3Item = m_mp3Items.at(index.row());
    switch (role) {
    case SelectedRole:
        return QVariant(mp3Item->selected);
    case DoneRole:
        return QVariant(mp3Item->download.finished());
    case UrlRole:
        return QVariant(mp3Item->download.url().toString());
    case ProgressRole:
        return QVariant(mp3Item->download.progress());
    case ErrorRole:
        return QVariant(mp3Item->download.error());
    }

    return QVariant();
}

bool Mp3ListModel::setData(const QModelIndex &index, const QVariant &value, int role) noexcept
{
    if (role == SelectedRole) {
        if (index.row() < 0 || index.row() >= m_mp3Items.size())
            return false;

        if (m_mp3Items.at(index.row())->selected == value.toBool())
            return false;

        m_mp3Items.at(index.row())->selected = value.toBool();
        dataChanged(index, index);
        return true;
    }

    return false;
}

Qt::ItemFlags Mp3ListModel::flags(const QModelIndex &index) const noexcept
{
    if (!index.isValid())
        return Qt::NoItemFlags;
    return Qt::ItemIsEditable;
}

void Mp3ListModel::addItem(const QString &urlStr) noexcept
{
    int numElements = 1;
    const int start = m_mp3Items.size();
    beginInsertRows(QModelIndex(), start, start + numElements - 1);

    Mp3Item *item = new Mp3Item(urlStr, m_downloadDir);
    m_mp3Items.append(item);

    endInsertRows();
}

void Mp3ListModel::removeCheckedItems() noexcept
{
    for (int i = 0; i < m_mp3Items.size();) {
        if (m_mp3Items.at(i)->selected) {
            beginRemoveRows(QModelIndex(), i, i);

            Mp3Item *item = m_mp3Items.takeAt(i);
            delete item;

            endRemoveRows();
        } else ++i;
    }
}

void Mp3ListModel::removeCompletedItems() noexcept
{
    for (int i = 0; i < m_mp3Items.size();) {
        if (m_mp3Items.at(i)->download.finished() || m_mp3Items.at(i)->download.error()) {
            beginRemoveRows(QModelIndex(), i, i);

            Mp3Item *item = m_mp3Items.takeAt(i);
            delete item;

            endRemoveRows();
        } else ++i;
    }
}

void Mp3ListModel::downloadCheckedItems() noexcept
{
    for (int i = 0; i < m_mp3Items.size(); ++i) {
        Mp3Item *item = m_mp3Items.at(i);

        if (item->selected && !item->download.finished() && !item->download.error() && double_equals(item->download.progress(), 0.0)) {

            connect(&item->download, &Download::downloadProgress, [this, i]() {
                dataChanged(index(i), index(i));
            });

            item->download.startDownload();
        }
    }
}

