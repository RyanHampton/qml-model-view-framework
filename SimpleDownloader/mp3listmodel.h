#ifndef MP3LISTMODEL_H
#define MP3LISTMODEL_H

#include <QAbstractListModel>
#include <QObject>
#include <QList>

#include "mp3item.h"

class Mp3List;

class Mp3ListModel : public QAbstractListModel
{
    Q_OBJECT

public:
    explicit Mp3ListModel(QUrl downloadDir, QObject *parent = nullptr) noexcept;

    enum {
        SelectedRole = Qt::UserRole,
        DoneRole,
        UrlRole,
        ProgressRole,
        ErrorRole
    };

    // QAbstractListModel virtual methods :
    virtual QHash<int, QByteArray> roleNames() const noexcept override;
    int rowCount(const QModelIndex &parent = QModelIndex()) const noexcept override;
    QVariant data(const QModelIndex &index, int role = Qt::DisplayRole) const noexcept override;
    bool setData(const QModelIndex &index, const QVariant &value, int role = Qt::EditRole) noexcept override;
    Qt::ItemFlags flags(const QModelIndex &index) const noexcept override;

public slots:
    void addItem(const QString &urlStr) noexcept;
    void removeCheckedItems() noexcept;
    void removeCompletedItems() noexcept;
    void downloadCheckedItems() noexcept;

private:
    QUrl m_downloadDir;
    QList<Mp3Item *> m_mp3Items;
};

#endif // MP3LISTMODEL_H
