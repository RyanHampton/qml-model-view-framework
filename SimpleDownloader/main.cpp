#include <QGuiApplication>
#include <QQmlApplicationEngine>
#include <QQuickStyle>
#include <QQmlContext>
#include <QStandardPaths>

#include <mp3listmodel.h>

int main(int argc, char *argv[])
{
    QCoreApplication::setAttribute(Qt::AA_DisableHighDpiScaling); // Bug with QtQuick.Dialogs and Scaling fof 4k monitors
    QQuickStyle::setStyle("Material");
    QGuiApplication app(argc, argv);
    app.setOrganizationName("ICS");
    app.setOrganizationDomain("ics.com");
    app.setApplicationName("QML Model-View Framework Webinar Example");

    QQmlApplicationEngine engine;

    QString localDownloadDir = QStandardPaths::writableLocation(QStandardPaths::DownloadLocation);
    if (localDownloadDir.isEmpty()) {
        localDownloadDir = QGuiApplication::applicationDirPath();
    }
    QUrl downloadDirUrl = QUrl::fromLocalFile(localDownloadDir);

    Mp3ListModel mp3ListModel(downloadDirUrl);
    engine.rootContext()->setContextProperty(QStringLiteral("mp3ListModel"), &mp3ListModel);
    qmlRegisterUncreatableType<Mp3ListModel>("DownloadManager", 1, 0, "Mp3ListModel",
                                             QStringLiteral("Mp3ListModel should not be created in QML"));

    QUrl url = QUrl(QStringLiteral("qrc:/main.qml"));

    // Must come before engine.load()
    QObject::connect(&engine, &QQmlApplicationEngine::objectCreated, &app,
                     [url](QObject *obj, const QUrl &objUrl) {
                         if (!obj && url == objUrl)
                             QCoreApplication::exit(-1);
                     },
                     Qt::QueuedConnection);

    engine.load(url); // Load blocks

    return app.exec();
}
