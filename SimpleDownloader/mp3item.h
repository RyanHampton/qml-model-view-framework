#ifndef MP3ITEM_H
#define MP3ITEM_H

#include <QString>
#include "download.h"

struct Mp3Item {
    explicit Mp3Item(QString url, QUrl downloadDir)
        : selected(true)
        , download(url, downloadDir)
    {}

    Mp3Item(const Mp3Item &) = delete;
    Mp3Item &operator=(const Mp3Item &) = delete;
    ~Mp3Item() = default;

    bool selected;
    Download download;
};

#endif // MP3ITEM_H
