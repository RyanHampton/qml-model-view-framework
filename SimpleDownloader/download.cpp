#include "download.h"

#include <QUrl>
#include <QNetworkRequest>
#include <QNetworkReply>
#include <QFile>
#include <QDir>
#include <QFileInfo>

Download::Download(QUrl url, QUrl downloadDir, QObject *parent)
    : QNetworkAccessManager(parent)
    , m_url(url)
    , m_downloadDir(downloadDir)
    , m_downloadedBytes(0)
    , m_totalBytes(0)
    , m_wasAborted(false)
    , m_error(false)
    , m_finished(false)
    , m_file(nullptr)
    , m_reply(nullptr)
{
}

void Download::startDownload() noexcept
{
    QString filename = m_downloadDir.path() + QString("/") + m_url.fileName();
    m_file = new QFile(filename, this);
    m_file->open(QFile::WriteOnly);
    startRequest(m_url);
}

void Download::startRequest(const QUrl &requestedUrl) noexcept
{
    m_url = requestedUrl;

    m_reply = get(QNetworkRequest(m_url));

    connect(m_reply, &QNetworkReply::finished, this, &Download::replyFinished);

    connect(m_reply, &QIODevice::readyRead, [this]() {
        if (m_file)
            m_file->write(m_reply->readAll());
    });

    connect(m_reply, &QNetworkReply::downloadProgress, [this](qint64 current, qint64 total) {
        m_downloadedBytes = current;
        m_totalBytes = total;
        emit downloadProgress();
    });

    connect(m_reply, qOverload<QNetworkReply::NetworkError>(&QNetworkReply::error),
            [this](QNetworkReply::NetworkError) { m_error = true; });
}

void Download::replyFinished() noexcept
{
    QFileInfo fi;
    if (m_file) {
        fi.setFile(m_file->fileName());
        m_file->deleteLater();
        m_file = nullptr;
    }

    if (m_reply->error()) {
        QFile::remove(fi.absoluteFilePath());
        m_reply->deleteLater();
        m_reply = nullptr;
        return;
    }
    const QVariant redirectionTarget = m_reply->attribute(QNetworkRequest::RedirectionTargetAttribute);

    m_reply->deleteLater();
    m_reply = nullptr;

    if (!redirectionTarget.isNull()) {
        const QUrl redirectedUrl = m_url.resolved(redirectionTarget.toUrl());
        m_file = new QFile(redirectedUrl.fileName(), this);
        m_file->open(QFile::WriteOnly);
        startRequest(redirectedUrl);
    }

    m_finished = true;
}

QUrl Download::url() const noexcept
{
    return m_url;
}

bool Download::finished() const noexcept
{
    return m_finished;
}

bool Download::error() const noexcept
{
    return m_error;
}

double Download::progress() const noexcept
{
    if (m_totalBytes > 0)
        return static_cast<double>(m_downloadedBytes) / static_cast<double>(m_totalBytes);
    return 0.0;
}

bool Download::wasAborted() const noexcept
{
    return m_wasAborted;
}

void Download::setWasAborted(bool wasAborted) noexcept
{
    m_wasAborted = wasAborted;
}

void Download::abortDownload() noexcept
{
    if (!m_finished)
        m_reply->abort();

    setWasAborted(true);
}
