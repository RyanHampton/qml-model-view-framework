#ifndef DOWNLOAD_H
#define DOWNLOAD_H

#include <QNetworkAccessManager>
#include <QNetworkReply>
#include <QUrl>
#include <QFile>

class Download : public QNetworkAccessManager
{
    Q_OBJECT
public:
    explicit Download(QUrl url, QUrl downloadDir, QObject *parent = nullptr);

    void startDownload() noexcept;
    void abortDownload() noexcept;

    bool wasAborted() const noexcept;
    void setWasAborted(bool wasAborted) noexcept;

    QUrl url() const noexcept;
    bool finished() const noexcept;
    bool error() const noexcept;
    double progress() const noexcept;

signals:
    void downloadProgress();

private slots:
    void replyFinished() noexcept;

private:
    void startRequest(const QUrl &requestedUrl) noexcept;

    QUrl m_url;
    QUrl m_downloadDir;
    qint64 m_downloadedBytes;
    qint64 m_totalBytes;
    bool m_wasAborted;
    bool m_error;
    bool m_finished;

    QFile *m_file;
    QNetworkReply *m_reply;
};

#endif // DOWNLOAD_H
