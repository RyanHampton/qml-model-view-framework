import QtQuick 2.12
import QtQuick.Window 2.12
import QtQuick.Controls.Material 2.12
import QtQuick 2.12
import QtQuick.Controls 2.5
import QtQuick.Layouts 1.12
import QtQuick.Dialogs 1.2

Window {
    visible: true
    width: 800
    minimumWidth: 900
    height: 640
    title: qsTr("MP3 Download Manager")

    Material.theme: Material.Dark
    Material.accent: Material.BlueGrey

    Rectangle {
        anchors.fill: parent
        color: "black";

        ColumnLayout{
            anchors.fill: parent
            anchors.margins: 20

            Frame {
                Layout.fillWidth: true
                Layout.fillHeight: true

                ListView {
                    anchors.fill: parent
                    model: mp3ListModel
                    delegate: Mp3Item {
                        width: parent.width
                    }
                }
            }

            RowLayout {
                spacing: 20

                Button {
                    text: qsTr("Download Checked")
                    icon.source: "qrc:/icons/Download-icon.png"
                    icon.color: "transparent"
                    onClicked: mp3ListModel.downloadCheckedItems()
                }

                Button{
                    text: qsTr("Remove Checked")
                    icon.source: "qrc:/icons/delete-icon.png"
                    icon.color: "transparent"
                    onClicked: mp3ListModel.removeCheckedItems()
                }

                Button{
                    text: qsTr("Remove Completed")
                    icon.source: "qrc:/icons/delete-icon.png"
                    icon.color: "transparent"
                    onClicked: mp3ListModel.removeCompletedItems()
                }
            }

            RowLayout {
                spacing: 20

                Button{
                    id: addButton
                    property int clickCount: 0
                    property variant urlList: ["https://www.free-stock-music.com/music/scott-buckley-ascension.mp3",
                                            "https://www.free-stock-music.com/music/scott-buckley-she-moved-mountains.mp3",
                                            "https://www.free-stock-music.com/music/tristan-lohengrin-bittersweet-season.mp3",
                                            "https://www.free-stock-music.com/music/serge-narcissoff-andromeda.mp3"]
                    text: qsTr("Add From URL")
                    icon.source: "qrc:/icons/Button-Add-icon.png"
                    icon.color: "transparent"
                    onClicked: {
                        mp3ListModel.addItem(urlTextField.text)
                        clickCount = (clickCount + 1) % 4
                    }
                }

                TextField {
                    id: urlTextField
                    Layout.fillWidth: true
                    text: addButton.urlList[addButton.clickCount]
                    selectByMouse: true
                }
            } // Add Row
        } // Column Layout
    } // Rectangle
}
