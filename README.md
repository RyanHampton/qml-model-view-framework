# QML Model View Framework

This project showcases the Model/View/Delegate examples explained in my ICS Blog post and corresponding webinar.

My project requires a c++14 compiler and Qt Version 5.12+.