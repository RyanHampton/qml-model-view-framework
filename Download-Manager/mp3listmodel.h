#ifndef MP3LISTMODEL_H
#define MP3LISTMODEL_H

#include <QAbstractListModel>

class Mp3List;

class Mp3ListModel : public QAbstractListModel
{
    Q_OBJECT

public:
    explicit Mp3ListModel(Mp3List *mp3List, QObject *parent = nullptr) noexcept;

    enum {
        SelectedRole = Qt::UserRole,
        DoneRole,
        UrlRole,
        ProgressRole,
        ErrorRole
    };

    // Basic functionality:
    int rowCount(const QModelIndex &parent = QModelIndex()) const noexcept override;

    QVariant data(const QModelIndex &index, int role = Qt::DisplayRole) const noexcept override;

    // Editable:
    bool setData(const QModelIndex &index, const QVariant &value,
                 int role = Qt::EditRole) noexcept override;

    Qt::ItemFlags flags(const QModelIndex &index) const noexcept override;

    virtual QHash<int, QByteArray> roleNames() const noexcept override;

private:
    Mp3List *m_list;
};

#endif // MP3LISTMODEL_H
