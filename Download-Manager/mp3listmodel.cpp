#include "mp3listmodel.h"
#include "mp3list.h"

Mp3ListModel::Mp3ListModel(Mp3List *mp3List, QObject *parent) noexcept
    : QAbstractListModel(parent), m_list(mp3List)
{
    if (m_list) {
        connect(m_list, &Mp3List::preItemsAppended, [this](int numElements) {
            const int start = m_list->items().size();
            beginInsertRows(QModelIndex(), start, start + numElements - 1);
        });

        connect(m_list, &Mp3List::postItemsAppended, [this]() {
            endInsertRows();
        });

        connect(m_list, &Mp3List::preItemRemoved, [this](int index) {
            beginRemoveRows(QModelIndex(), index, index);
        });

        connect(m_list, &Mp3List::postItemRemoved, [this]() {
            endRemoveRows();
        });

        connect(m_list, &Mp3List::dataChanged, [this](int i) {
            dataChanged(index(i), index(i));
        });
    }
}

int Mp3ListModel::rowCount(const QModelIndex &parent) const noexcept
{
    if (parent.isValid() || m_list == nullptr)
        return 0;

    return m_list->items().size();
}

QVariant Mp3ListModel::data(const QModelIndex &index, int role) const noexcept
{
    if (!index.isValid() || m_list == nullptr)
        return QVariant();

    Mp3Item *mp3Item = m_list->items().at(index.row());
    switch (role) {
    case SelectedRole:
        return QVariant(mp3Item->selected);
    case DoneRole:
        return QVariant(mp3Item->download.finished());
    case UrlRole:
        return QVariant(mp3Item->download.url().toString());
    case ProgressRole:
        return QVariant(mp3Item->download.progress());
    case ErrorRole:
        return QVariant(mp3Item->download.error());
    }

    return QVariant();
}

bool Mp3ListModel::setData(const QModelIndex &index, const QVariant &value, int role) noexcept
{
    if (m_list == nullptr)
        return false;

    if (role == SelectedRole) {
        return m_list->updateSelectionValue(index.row(), value.toBool());
    }

    return false;
}

Qt::ItemFlags Mp3ListModel::flags(const QModelIndex &index) const noexcept
{
    if (!index.isValid())
        return Qt::NoItemFlags;

    return Qt::ItemIsEditable;
}

QHash<int, QByteArray> Mp3ListModel::roleNames() const noexcept
{
    QHash<int, QByteArray> roles;
    roles[SelectedRole] = "selected";
    roles[DoneRole] = "done";
    roles[ProgressRole] = "progress";
    roles[UrlRole] = "url";
    roles[ErrorRole] = "error";
    return roles;
}
