#ifndef MP3LIST_H
#define MP3LIST_H

#include <QObject>
#include <QVariant>
#include <QList>

#include "download.h"

struct Mp3Item {
    explicit Mp3Item(QString url, QUrl downloadDir)
        : selected(true)
        , download(url, downloadDir)
    {
    }
    Mp3Item(const Mp3Item &) = delete;
    Mp3Item &operator=(const Mp3Item &) = delete;
    ~Mp3Item() = default;

    bool selected;
    Download download;
};

class Mp3List : public QObject
{
    Q_OBJECT

    Q_PROPERTY(QStringList playlistUrls MEMBER m_playlistUrls CONSTANT)

    Q_PROPERTY(QUrl downloadDir READ downloadDir WRITE setDownloadDir NOTIFY downloadDirChanged)

public:
    explicit Mp3List(QUrl downloadDir, QObject *parent = nullptr) noexcept;

    QList<Mp3Item *> items() const noexcept;

    bool updateSelectionValue(int index, bool selectionValue) noexcept;

    QUrl downloadDir() const noexcept;
    void setDownloadDir(const QUrl &downloadDir) noexcept;

signals:
    void preItemsAppended(int numElements);
    void postItemsAppended();

    void preItemRemoved(int index);
    void postItemRemoved();

    void dataChanged(int index);

    void downloadDirChanged();

public slots:
    void addEntirePlaylist() noexcept;
    void addItem(const QString &urlStr) noexcept;
    void removeCheckedItems() noexcept;
    void removeCompletedItems() noexcept;
    void downloadCheckedItems() noexcept;

private:
    QStringList m_playlistUrls;
    QList<Mp3Item *> m_mp3Items;
    QUrl m_downloadDir;
};

#endif // MP3LIST_H
