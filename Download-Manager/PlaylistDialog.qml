import QtQuick 2.12
import QtQuick.Controls 2.5
import QtQuick.Dialogs 1.2

Dialog {
    title: qsTr("Playlist Information")
    standardButtons: StandardButton.Ok
    width: 600
    height: 300

    Text{
        id: titleText
        text: qsTr("Automatically add the following MP3's to be downloaded:")
    }

    ListView {
        anchors.top: titleText.bottom
        anchors.bottom: parent.bottom
        anchors.topMargin: 50

        model: mp3List.playlistUrls

        delegate: Text {
            text: modelData
        }
    }

}
