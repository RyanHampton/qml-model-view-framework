import QtQuick 2.12
import QtQuick.Controls 2.5
import QtQuick.Layouts 1.12
import DownloadManager 1.0
import QtQuick.Dialogs 1.2

ColumnLayout{

    Frame {
        Layout.fillWidth: true
        Layout.fillHeight: true

        ListView {
            anchors.fill: parent
            model: mp3ListModel
            delegate: Mp3Item {
                width: parent.width
            }

        }
    }

    RowLayout {
        spacing: 20

        Button {
            text: qsTr("Download Checked")
            icon.source: "qrc:/icons/Download-icon.png"
            icon.color: "transparent"
            onClicked: mp3List.downloadCheckedItems()
        }

        Button{
            text: qsTr("Remove Checked")
            icon.source: "qrc:/icons/delete-icon.png"
            icon.color: "transparent"
            onClicked: mp3List.removeCheckedItems()
        }

        Button{
            text: qsTr("Remove Completed")
            icon.source: "qrc:/icons/delete-icon.png"
            icon.color: "transparent"
            onClicked: mp3List.removeCompletedItems()
        }
    }

    RowLayout {
        spacing: 20

        Button{
            text: qsTr("Download Folder")
            icon.source: "qrc:/icons/Places-folder-music-icon.png"
            icon.color: "transparent"
            onClicked: fileDialog.open()
        }

        Label {
            Layout.fillWidth: true
            text: mp3List.downloadDir
            elide: Text.ElideLeft
        }
    }

    RowLayout {
        spacing: 20

        Button{
            text: qsTr("Add From URL")
            icon.source: "qrc:/icons/Button-Add-icon.png"
            icon.color: "transparent"
            onClicked: mp3List.addItem(urlTextField.text)
        }

        TextField {
            id: urlTextField
            Layout.fillWidth: true
            text: "https://www.free-stock-music.com/music/scott-buckley-ascension.mp3"
            selectByMouse: true
        }
    }

    RowLayout {
        spacing: 20

        Button{
            text: qsTr("Add Entire Playlist")
            icon.source: "qrc:/icons/Folder-Add-icon.png"
            icon.color: "transparent"
            onClicked: mp3List.addEntirePlaylist()
        }

        Button {
            text: qsTr("Playlist Info")
            icon.source: "qrc:/icons/Help-icon.png"
            icon.color: "transparent"
            onClicked: playlistDialog.open()
        }
    }

    PlaylistDialog {
        id: playlistDialog
        visible: false
    }

    FileDialog {
        id: fileDialog
        visible: false
        title: "Choose a download directory"
        folder: mp3List.downloadDir
        selectFolder: true
        onAccepted: mp3List.downloadDir = folder
    }

}
