#include <QDir>

#include "mp3list.h"

static bool double_equals(double a, double b, double epsilon = 0.0001)
{
    return std::abs(a - b) < epsilon;
}

Mp3List::Mp3List(QUrl downloadDir, QObject *parent) noexcept
    : QObject(parent)
    , m_downloadDir(downloadDir)
{
    m_playlistUrls.append(QStringLiteral("https://www.free-stock-music.com/music/scott-buckley-ascension.mp3"));
    m_playlistUrls.append(QStringLiteral("https://www.free-stock-music.com/music/scott-buckley-she-moved-mountains.mp3"));
    m_playlistUrls.append(QStringLiteral("https://www.free-stock-music.com/music/tristan-lohengrin-bittersweet-season.mp3"));
    m_playlistUrls.append(QStringLiteral("https://www.free-stock-music.com/music/serge-narcissoff-andromeda.mp3"));
    m_playlistUrls.append(QStringLiteral("https://www.free-stock-music.com/music/alexander-nakarada-vopna.mp3"));
}

bool Mp3List::updateSelectionValue(int index, bool selectionValue) noexcept
{
    if (index < 0 || index >= m_mp3Items.size())
        return false;

    if (m_mp3Items.at(index)->selected == selectionValue)
        return false;

    m_mp3Items.at(index)->selected = selectionValue;
    emit dataChanged(index);
    return true;
}

QList<Mp3Item *> Mp3List::items() const noexcept
{
    return m_mp3Items;
}

QUrl Mp3List::downloadDir() const noexcept
{
    return m_downloadDir;
}

void Mp3List::setDownloadDir(const QUrl &downloadDir) noexcept
{
    if (downloadDir != m_downloadDir) {
        m_downloadDir = downloadDir;
        emit downloadDirChanged();
    }
}

void Mp3List::addItem(const QString &urlStr) noexcept
{
    emit preItemsAppended(1);

    Mp3Item *item = new Mp3Item(urlStr, m_downloadDir);
    m_mp3Items.append(item);

    emit postItemsAppended();
}

void Mp3List::addEntirePlaylist() noexcept
{
    emit preItemsAppended(m_playlistUrls.size());

    for (QString url : m_playlistUrls) {
        Mp3Item *item = new Mp3Item(url, m_downloadDir);
        m_mp3Items.append(item);
    }

    emit postItemsAppended();
}

void Mp3List::removeCheckedItems() noexcept
{
    for (int i = 0; i < m_mp3Items.size();) {
        if (m_mp3Items.at(i)->selected) {
            emit preItemRemoved(i);

            Mp3Item *item = m_mp3Items.takeAt(i);
            delete item;

            emit postItemRemoved();
        } else {
            ++i;
        }
    }
}

void Mp3List::removeCompletedItems() noexcept
{
    for (int i = 0; i < m_mp3Items.size();) {
        if (m_mp3Items.at(i)->download.finished() || m_mp3Items.at(i)->download.error()) {
            emit preItemRemoved(i);

            Mp3Item *item = m_mp3Items.takeAt(i);
            delete item;

            emit postItemRemoved();
        } else {
            ++i;
        }
    }
}

void Mp3List::downloadCheckedItems() noexcept
{
    for (int i = 0; i < m_mp3Items.size(); ++i) {
        Mp3Item *item = m_mp3Items.at(i);

        if (item->selected && !item->download.finished() && !item->download.error() && double_equals(item->download.progress(), 0.0)) {

            connect(&item->download, &Download::downloadProgress, [this, i]() { emit dataChanged(i); });

            item->download.startDownload();
        }
    }
}
