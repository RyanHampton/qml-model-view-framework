# Qt version check for quickControls2
contains(QT_VERSION, ^5\\.(([0-9])|(10)|(11))\\..*) {
    message("Cannot build with Qt version $${QT_VERSION}.")
    error("Use at least Qt 5.12")
}

QT += quick quickcontrols2
CONFIG += c++14

DEFINES += QT_DISABLE_DEPRECATED_BEFORE=0x060000

SOURCES += \
        download.cpp \
        main.cpp \
        mp3list.cpp \
        mp3listmodel.cpp

HEADERS += \
    download.h \
    mp3list.h \
    mp3listmodel.h

RESOURCES += qml.qrc

qnx: target.path = /tmp/$${TARGET}/bin
else: unix:!android: target.path = /opt/$${TARGET}/bin
!isEmpty(target.path): INSTALLS += target
