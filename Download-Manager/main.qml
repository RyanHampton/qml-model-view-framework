import QtQuick 2.12
import QtQuick.Window 2.12
import QtQuick.Controls.Material 2.12

Window {
    visible: true
    width: 800
    minimumWidth: 800
    height: 640
    title: qsTr("MP3 Download Manager")

    Material.theme: Material.Dark
    Material.accent: Material.BlueGrey

    Rectangle {
        anchors.fill: parent
        color: "black";

        DownloadManager {
            anchors.fill: parent
            anchors.margins: 20
        }
    }

}
